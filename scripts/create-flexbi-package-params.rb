# Your username for Stash
$username = "osteinbaums"
$flexbi_version = "4.0"

#The filename of the file that will be downloaded and unzipped
$eazybi_filename = "eazybi_private-4.1.0.zip"

#The download link for the file
$eazybi_url = "https://eazybi.com/system/downloads/"#eazybi_private-3.3.0.zip"

#Git repository you want to clone
$clone_what = "https://#{$username}@services.burti.lv/stash/scm/flexbi/flexbi.git"

#Branch and repository from which you want to pull 
$repository = "origin"
$branch = "develop-cloud"

#A list of file PATHS in relation to the script to be deleted (excluded)
$exclude = [
'flexbi',									#example of a directory sharing the same directory as the script
'.git',
'flexbi_private'
#$eazybi_filename
#'flexbi_private/flexbi_private/app',		#example of a directory path
#'flexbi_private/flexbi_private/CHANGELOG.md' 	#example of a filepath
]
