$eazybi_filename, $repository, $branch = ARGV

require 'rubygems'
require 'zip'
require 'net/http'
require 'fileutils'
require 'mkmf'
require_relative 'create-flexbi-package-params.rb'

class RedirectFollower
  class TooManyRedirects < StandardError; end

  attr_accessor :url, :body, :redirect_limit, :response

  def initialize(url, limit=5)
    @url, @redirect_limit = url, limit
  end

  def resolve
    raise TooManyRedirects if redirect_limit < 0

    self.response = Net::HTTP.get_response(URI.parse(url))

    if response.kind_of?(Net::HTTPRedirection)      
      self.url = redirect_url
      self.redirect_limit -= 1

      resolve
    end

    self.body = response.body
    self
  end

  def redirect_url
    if response['location'].nil?
      response.body.match(/<a href=\"([^>]+)\">/i)[1]
    else
      response['location']
    end
  end
end

def download(file_url, filename)
		link = RedirectFollower.new(file_url).resolve
		puts "Downloaded from: #{link.url}"
		File.open(filename, "w") do |file|
  		file.write link.body	
  	end
end

def download_zip(filename, file_url)
	puts "Downloading #{filename}"
	download(file_url, filename)
end

def unzip_file(filename, file_path)
	puts "Unzipping #{filename}."
	Zip::File.open(filename) { |zip_file|
     	zip_file.each { |f|
     		f_path=File.join(file_path , f.name)
     		FileUtils.mkdir_p(File.dirname(f_path))
     		zip_file.extract(f, f_path) unless File.exist?(f_path)
  		}
  	}

  	puts "#{filename} unzipped to #{file_path}."
  	File.delete(filename)
  	puts "#{filename} deleted."
end

def convert_to_class
	puts "Compiling all .rb files in eazybi_private"
	system("jrubyc eazybi_private/**/*.rb")
end

def alter_rb_files 	
	puts "Replacig content of .rb files"
	Dir.glob('eazybi_private/**/*.rb') do |rb_file|
		File.open(rb_file, 'w') {|file| file.write("load __FILE__.sub(/\\.rb$/, '.class')")}
	end
end

def remove_class
	exec('find . -name "*.class" -type f|xargs rm -f')
end

def get_git(clone_what, repository, branch)
	system('git init')
	system("git clone #{clone_what}")
	Dir.chdir "flexbi"
	system("git checkout #{branch}")
	$hash_string = %x[git rev-parse #{repository}/#{branch}]
	Dir.chdir ".."
end

def change_file_content 
	puts "Done replacing content of .rb files"
	puts "Renaming eazybi_private to flexbi_private"
	File.rename("eazybi_private", "flexbi_private")
	Dir.chdir "flexbi_private"
	File.rename("eazybi_private", "flexbi_private")
	puts "Renaming flexbi_private/public/bi to flexbi_private/public/flexbi"
	Dir.chdir "flexbi_private"
	Dir.chdir "bin"
	File.rename("start.sh", "startsh.txt")
	File.rename("start.bat", "startbat.txt")

	text = File.read('startsh.txt')
	new_contents = text.gsub(/\$EAZYBI_HOME\/eazybi_private.jar/, "$EAZYBI_HOME/flexbi_private.jar")
	File.open('startsh.txt', "w") {|file| file.puts new_contents }

	text = File.read('startsh.txt')
	new_contents = text.gsub(/\# export EAZYBI_PREFIX=\/eazybi/, "export EAZYBI_PREFIX=/flexbi")
	File.open('startsh.txt', "w") {|file| file.puts new_contents }

	text = File.read('startbat.txt')
	new_contents = text.gsub(/%EAZYBI_HOME%\\eazybi_private.jar/, "%EAZYBI_HOME%\\flexbi_private.jar")
	File.open('startbat.txt', "w") {|file| file.puts new_contents }

	text = File.read('startbat.txt')
	new_contents = text.gsub(/rem set "EAZYBI_PREFIX=\/eazybi"/, "set \"EAZYBI_PREFIX=/flexbi\"")
	File.open('startbat.txt', "w") {|file| file.puts new_contents }
	
	File.rename("startsh.txt", "start.sh")
	File.rename("startbat.txt", "start.bat")
	
	Dir.chdir ".."
	Dir.chdir ".."
	Dir.chdir ".."
end

def alter_directories
	if File.exists?("eazybi_private/eazybi_private/flexbi_private.jar") == false
		puts "Renaming eazybi_private.jar to flexbi_private.jar ..."
		File.rename("eazybi_private/eazybi_private/eazybi_private.jar", "eazybi_private/eazybi_private/flexbi_private.jar")
	end
	
	if File.exists?("eazybi_private/eazybi_private/bin/init.d/flexbi.sample") == false
		puts "Renaming eazybi.sample to flexbi.sample ..."
		File.rename("eazybi_private/eazybi_private/bin/init.d/eazybi.sample", "eazybi_private/eazybi_private/bin/init.d/flexbi.sample")
	end

	puts "Copying app, public, spec folders ..."
	FileUtils.cp_r("flexbi/eazybi_private/app", 'eazybi_private/eazybi_private', :verbose => true)
	FileUtils.cp_r("flexbi/eazybi_private/public", 'eazybi_private/eazybi_private', :verbose => true)
	FileUtils.cp_r("flexbi/eazybi_private/spec", 'eazybi_private/eazybi_private', :verbose => true)
	if $flexbi_version.gsub(".", "").to_i >= 40
		puts "Copying vendor, db folders... "
		FileUtils.cp_r("flexbi/eazybi_private/vendor", 'eazybi_private/eazybi_private', :verbose => true)
		FileUtils.cp_r("flexbi/eazybi_private/db", 'eazybi_private/eazybi_private', :verbose => true)
	end
	puts "Copying files from /config"
	FileUtils.cp_r("flexbi/eazybi_private/config/database.toml.sample", 'eazybi_private/eazybi_private/config/', :verbose => true)
	FileUtils.cp_r("flexbi/eazybi_private/config/eazybi.toml.sample", 'eazybi_private/eazybi_private/config/', :verbose => true)
	FileUtils.cp_r("flexbi/eazybi_private/config/ldap.toml.sample", 'eazybi_private/eazybi_private/config/', :verbose => true)
	FileUtils.cp_r("flexbi/eazybi_private/config/initializers", 'eazybi_private/eazybi_private/config/', :verbose => true)
	FileUtils.cp_r("flexbi/eazybi_private/config/locales", 'eazybi_private/eazybi_private/config/', :verbose => true)
	FileUtils.cp_r("flexbi/eazybi_private/config/torquebox", 'eazybi_private/eazybi_private/config/', :verbose => true)
end

def zip_up
	puts "Zipping up flexbi_private"
	Dir.chdir "flexbi_private"
	Dir["flexbi_private"].each do |file|
 		if File.directory?(file)
    	`zip -r "#{file}.zip" "#{file}"`
  		end
	end
	Dir.chdir ".."
	working_d = %x[pwd]
	FileUtils.mv("flexbi_private/flexbi_private.zip", ".")
	eazybi_regxp = /\d.+[^.zip]/  
	m = eazybi_regxp.match($eazybi_filename)  
	File.rename("flexbi_private.zip", "flexbi_private_ezbi-#{m[0]}_#{$hash_string[0..10]}.zip")
end

def cleanup
	puts "Performing cleanup"
	$exclude.each do |file|
		FileUtils.rm_rf(file)
	end
end

if find_executable 'jrubyc'
  fileurl = $eazybi_url + $eazybi_filename
  download_zip($eazybi_filename, fileurl)
  unzip_file($eazybi_filename, "eazybi_private")
  get_git($clone_what, $repository, $branch)
  alter_directories
  convert_to_class
  alter_rb_files
  change_file_content
  zip_up
  cleanup
end

puts "Script finished"






